set nocompatible
filetype off

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
" Actually we'll homeshick will manage Vundle
"Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
"Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
"Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
"Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
"Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}

"Bundle 'altercation/vim-colors-solarized'
"Bundle 'https://bitbucket.org/edowd/vim-colors-solarized.git'
Plugin 'https://bitbucket.org/edowd/vim-colors-solarized.git'

"Bundle 'tpope/vim-markdown'
"Bundle 'tpope/vim-liquid'
"Bundle 'ericdowd/vim-liquid'
"Bundle 'https://bitbucket.org/edowd/vim-liquid.git'
Plugin 'https://bitbucket.org/edowd/vim-liquid.git'

" All of your Plugins must be added before the following line
call vundle#end()            " required
"filetype plugin indent on    " required
filetype plugin on
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
"
syntax enable

set tabstop=4
set shiftwidth=4
set expandtab

if has('gui_running')
    set background=light
"    set guifont=Consolas:h12
else
    set background=dark
endif

colorscheme solarized

"set backspace=indent,eol,start

set nofoldenable
set foldlevelstart=32767

set ruler

map <F6> :setlocal spell! spelllang=en_us<CR>
map <F7> :setlocal autoindent!<CR>

