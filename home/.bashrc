# /etc/skel/.bashrc
#
# This file is sourced by all *interactive* bash shells on startup,
# including some apparently interactive shells such as scp and rcp
# that can't tolerate any output.  So make sure this doesn't display
# anything or bad things will happen !


# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	return
fi


# Put your fun stuff here.

[[ -f ~/.homesick/repos/homeshick/homeshick.sh ]] && . ~/.homesick/repos/homeshick/homeshick.sh
[[ -f ~/.homesick/repos/homeshick/completions/homeshick-completion.bash ]] && . ~/.homesick/repos/homeshick/completions/homeshick-completion.bash

export EDITOR=vim

export TERM="xterm-256color"

UNAME=`uname`
if [[ $UNAME == "Linux" ]]; then
alias ls="ls --color -F"
elif [[ $UNAME == "Darwin" ]]; then
alias ls="ls -GpF"
export DYLD_LIBRARY_PATH=/usr/local/lib:/usr/lib:$DYLD_LIBRARY_PATH
fi

export PS1="\[\033[00;32m\]\u@\h\[\033[00;34m\] \w \$\[\033[00m\] "

# RVM
[[ -f ~/.rvm/scripts/rvm ]] && . ~/.rvm/scripts/rvm
if [[ -d $HOME/.rvm/bin ]]; then
    PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
fi

# Bash Completion
if [[ -f /etc/profile.d/bash-completion.sh ]] && ! shopt -oq posix; then
    . /etc/profile.d/bash-completion.sh
elif [[ -f /etc/bash-completion ]] && ! shopt -oq posix; then
    . /etc/bash-completion
fi

# virtualenv
export WORKON_HOME=$HOME/.virtualenvs
[[ -f /usr/bin/virtualenvwrapper_lazy.sh ]] && . /usr/bin/virtualenvwrapper_lazy.sh
[[ -f /usr/local/bin/virtualenvwrapper_lazy.sh ]] && . /usr/local/bin/virtualenvwrapper_lazy.sh
[[ -f /usr/share/virtualenvwrapper/virtualenvwrapper_lazy.sh ]] && . /usr/share/virtualenvwrapper/virtualenvwrapper_lazy.sh

# Alias definition
[[ -f ~/.bash_aliases ]] && . ~/.bash_aliases

# AWS Credentials
[[ -f ~/.bash_aws ]] && . ~/.bash_aws

